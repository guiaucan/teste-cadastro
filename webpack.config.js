const

	// Node modules

	path = require('path'),

	// Webpack plugins

	HTMLWebpackPlugin = require('html-webpack-plugin'),

	CleanWebpackPlugin = require('clean-webpack-plugin'),

	ExtractTextWebpackPlugin = require('extract-text-webpack-plugin'),

	// Babel plugins

	babelPluginProposalObjectRestSpread = require('@babel/plugin-proposal-object-rest-spread'),
	
	babelPluginSyntaxDynamicImport = require('@babel/plugin-syntax-dynamic-import'),

	// PostCSS plugins

	PreCSS = require('precss'),

	AutoPrefixer = require('autoprefixer');

let extractCSS = new ExtractTextWebpackPlugin('[name].css');

module.exports = [
	{
		target: 'web',
		context: path.resolve(__dirname, 'source'),
		entry: {
			index: [
				'./index.js',
				'./index.scss'
			]
		},
		output: {
			path: path.resolve(__dirname, 'distribution')
		},
		module: {
			rules: [
				{
					test: /\.s?css$/,
					exclude: [
						/node_modules/,
						/^_/
					],
					use: extractCSS.extract({
						use: [
							{
								loader: 'css-loader',
								options: {
									minimize: true
								}
							},
							{
								loader: 'postcss-loader',
								options: {
									plugins: () => [
										PreCSS,
										AutoPrefixer
									]
								}
							},
							{
								loader: 'sass-loader',
								options: {
									includePaths: ['node_modules']
								}
							}
						]
					})
				},
				{
					test: /\.(ico|png|jpg|gif|woff2?|eot|ttf|svg)$/,
					loader: 'url-loader',
					options: {
						name: '[path][hash].[ext]',
						limit: 8192
					}
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								presets: [
									'@babel/preset-env'
								],
								plugins: [
									babelPluginProposalObjectRestSpread,
									babelPluginSyntaxDynamicImport
								]
							}
						}
					]
				},
				{
					test: /\.html$/,
					exclude: [
						/node_modules/,
						path.resolve(__dirname, 'source', 'index.html')
					],
					use: [
						{
							loader: 'file-loader',
							options: {
								name: '[path][name].[ext]'
							}
						},
						{
							loader: 'extract-loader'
						},
						{
							loader: 'html-loader',
							options: {
								minimize: true,
								attrs: [
									'img:src',
									'link:href'
								],
								interpolate: true,
								root: path.resolve(__dirname, 'source', 'application')
							}
						}
					]
				}
			]
		},
		devtool: 'source-map',
		resolve: {
			extensions: [
				'.js'
			],
			modules: [
				'node_modules'
			],
			alias: {
				application: path.resolve(__dirname, 'source', 'application')
			}
		},
		devServer: {
			contentBase: path.resolve(__dirname, 'distribution'),
			port: 3000,
			open: true,
			historyApiFallback: true
		},
		plugins: [
			new CleanWebpackPlugin([
				path.resolve(__dirname, 'distribution')
			]),
			new HTMLWebpackPlugin({
				hash: true,
				template: path.resolve(__dirname, 'source', 'index.html'),
				path: path.resolve(__dirname, 'distribution'),
				filename: 'index.html',
				xhtml: true,
				minify: {
					collapseWhitespace: true,
					caseSensitive: true
				},
				favicon: path.resolve(__dirname, 'source', 'application', 'assets', 'images', 'favicon.ico')
			}),
			extractCSS
		]
	}
];
