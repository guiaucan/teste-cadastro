import 'application/config';

import 'application/routes';

import 'application/run';

import 'jquery';

import 'bootstrap/js/dist/modal';

import 'bootstrap/js/dist/toast';

import 'bootstrap/js/dist/alert';

import '@fortawesome/fontawesome-free/js/all';

import '@fortawesome/fontawesome-free/js/regular';

import '@fortawesome/fontawesome-free/js/solid';
