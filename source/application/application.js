import angular from 'angular';

import angularUIRouter from '@uirouter/angularjs';

import angularFilter from 'angular-filter';

export default angular.module('teste', [
	angularUIRouter,
	angularFilter
]);
