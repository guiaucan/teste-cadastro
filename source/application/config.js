import application from 'application/application';

application

	.config(['$locationProvider', '$urlRouterProvider', ($locationProvider, $urlRouterProvider) => {

		$locationProvider.html5Mode(true);

		$urlRouterProvider

			.rule(($injector, $location) => {

				let path = $location.path();

				if (path.substr(-1) === '/')

					return path.substr(0, path.length - 1);

			})

			.otherwise(($injector) => {

				$injector

					.get('$state')

					.go('root.default.initial', null, {
						location: false
					});

			});

	}]);
