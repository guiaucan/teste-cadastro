import application from 'application/application';

import template from './default.template.html';

import controller from './default.controller';

application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

			.state('root.default', {
				abstract: true,
				views: {
					'root': {
						templateUrl: template,
						controller
					}
				},
				params: {
					authenticate: true
				}
			});

	}]);
