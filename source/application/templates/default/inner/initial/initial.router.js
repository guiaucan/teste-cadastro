import application from 'application/application';

import template from './initial.template.html';

import controller from './initial.controller';

application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

			.state('root.default.initial', {
				url: '/',
				views: {
					'default@root.default': {
						templateUrl: template,
						controller
					}
				},
				data: {
					documentTitle: 'Jogo da memória | Inicio'
				}
			});

	}]);
