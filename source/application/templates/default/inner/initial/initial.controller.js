import application from 'application/application';

import $ from 'jquery';

import 'bootstrap-notify';

const controller = 'DefaultInitialController';

application

	.controller(controller, ($scope, $timeout, userService) => {
		$scope.users = JSON.parse(localStorage.getItem('users')) || [];

		$scope.saveItem = (() => {
			if (userService.isValid($scope.newUser)) {
				$scope.newUser.id = new Date().getTime();

				$scope.users.push($scope.newUser);

				userService.writeUser(angular.copy($scope.users));

				$scope.newUser = {};

				$.notify({
					message: 'Cadastro realizado com sucesso.' 
				},{
					type: 'success'
				});

				$('#newUser').modal('hide');
			}
		});

		$('#newUser').on('hide.bs.modal', () => {
			$scope.newUser = {};

			$timeout(() => $scope.$apply('newUser'));
			
		});

		$scope.edit = obj => {
			$scope.editUser = obj;

			$('#editUser').modal('show');
		};

		$('#editUser').on('hide.bs.modal', () => {
			$scope.editUser = {};

			$timeout(() => $scope.$apply('editUser'));
		});

		$scope.sendEditForm = obj => {
			if (userService.isValid(obj)) {
				$scope.users.map((item, key) => {
					if (item.id == obj.id)
						$scope.users[key] = { ...item, ...obj }
				});
			}

			userService.writeUser(angular.copy($scope.users));

			$.notify({
				message: 'O item foi editado com sucesso.' 
			},{
				type: 'success'
			});

			$timeout(() =>$('#editUser').modal('hide'));
			
		};

		$scope.deleteUser = (id => {

			$('#deleteUser').modal('show');

			$('#deleteUser .delete').on('click', () => {
				$scope.users.map((item, key) => {
					if (item && item.id == id)
						$scope.users.splice(key, 1);
				});
	
				userService.writeUser(angular.copy($scope.users));

				$scope.$apply('users');

				$.notify({
					message: 'O item foi deletado com sucesso.' 
				},{
					type: 'success'
				});

				$('#deleteUser').modal('hide');
			});
			
		});
	});

export default controller;
