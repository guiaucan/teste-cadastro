import application from 'application/application';

export default application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

			.state('root', {
				abstract: true,
				template: '<ui-view name="root"></ui-view>'
			});

	}]);
