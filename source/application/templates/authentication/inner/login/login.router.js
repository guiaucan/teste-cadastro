import application from 'application/application';

import template from './login.template.html';

import controller from './login.controller';

application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

		.state('root.authentication.login', {
			url: '/login',
			views: {
				'authentication@root.authentication': {
					templateUrl: template,
					controller
				}
			},
			data: {
				documentTitle: 'Teste | Login'
			}
		});

	}]);
