import application from 'application/application';

import $ from 'jquery';

const controller = 'authenticationLoginController';

application

	.controller(controller, ($scope, $location, $timeout) => {

		const users = JSON.parse(localStorage.getItem('users')) || [];

		$scope.login = (obj => {
			let correct = false;

			users.map(item => {
				if (obj && (item.email == obj.email && item.senha == obj.senha)) {
					correct = true;
				}
			});

			if(correct) {
				localStorage.setItem('logado', true);

				return $location.path('/');
			}

			$('body').append('<div class="alert alert-warning" role="alert">A simple warning alert—check it out! </div>');

			// $('.toast').toast('show');
		});

	});

export default controller;
