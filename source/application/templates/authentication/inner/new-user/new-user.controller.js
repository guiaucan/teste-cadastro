import application from 'application/application';

import $ from 'jquery';

import 'bootstrap-notify';

const controller = 'LoginController';

application

	.controller(controller, ($scope, $location, userService) => {

		const
			users = $scope.users = JSON.parse(localStorage.getItem('users')) || [];

		$scope.saveItem = (() => {
			if (userService.isValid($scope.newUser)) {
				$scope.newUser.id = new Date().getTime();

				users.push($scope.newUser);

				userService.writeUser(users);

				$scope.newUser = {};

				$location.path('/login');
			}
		});

	})

	.service('userService', function () {
		this.getUsers = () => { this.users = JSON.parse(localStorage.getItem('users')) || [] };

		this.getUsers();

		this.usersSchema = {
			nome: {
				required: 'Campo nome é obrigatório'
			},
			email: {
				required: 'Campo e-mail é obrigatório'
			}, 
			telefone: {
				required: 'Campo telefone é obrigatório'
			}, 
			cpf: {
				required: 'Campo CPF é obrigatório'
			},
			senha: {
				required: 'Campo senha é obrigatório'
			}
		};

		this.isValid = (obj => {
			this.getUsers();

			let error = true;

			if(obj && obj.email)
				this.users.map(item => {
					try {
						console.log(obj, item)
						if (!obj.id && item.email == obj.email)
							throw 'O e-mail %s já está cadastrado';
					} catch (err) {
						error = false;

						$.notify({
							message: err.replace('%s', obj.email)
						},{
							type: 'danger'
						});
					}
				});

			for (let key in this.usersSchema) {
				try { 
					if (!obj || (this.usersSchema[key].required && !obj[key]))
						throw this.usersSchema[key].required;
				} catch (err) {
					error = false;
					$.notify({
						message: err
					},{
						type: 'danger'
					});
				}
			};

			return error;
		});

		this.writeUser = obj => {
			localStorage.setItem('users', JSON.stringify(obj));
		};
	});

export default controller;
