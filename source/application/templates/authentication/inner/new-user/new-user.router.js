import application from 'application/application';

import template from './new-user.template.html';

import controller from './new-user.controller';

application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

		.state('root.authentication.new-user', {
			url: '/novo-usuario',
			views: {
				'authentication@root.authentication': {
					templateUrl: template,
					controller
				}
			},
			data: {
				documentTitle: 'Teste | Login'
			}
		});

	}]);
