import application from 'application/application';

import template from './authentication.template.html';

import controller from './authentication.controller';

application

	.config(['$stateProvider', $stateProvider => {

		$stateProvider

		.state('root.authentication', {
			abstract: true,
			views: {
				'root': {
					templateUrl: template,
					controller
				}
			},
			params: {
				authenticate: false
			}
		});

	}]);
