import 'application/templates/root.router';

import 'application/templates/default/default.router';

import 'application/templates/default/inner/initial/initial.router';

import 'application/templates/authentication/authentication.router';

import 'application/templates/authentication/inner/login/login.router';

import 'application/templates/authentication/inner/new-user/new-user.router';
