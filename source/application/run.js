import application from 'application/application';

application

	.run(['$rootScope', '$state', '$transitions', '$location', '$timeout', ($rootScope, $state, $transitions, $location, $timeout) => {

		// $rootScope.basicInfo = {
		// 	name: undefined
		// };

		$rootScope.logout = () => {
			localStorage.removeItem('logado');
			$location.path('/login');
		};


		// firebase.initializeApp({
		// 	apiKey: "AIzaSyAJi-mgcsbB2mAHBgRE4p5XUycj3i7x6-E",
		// 	authDomain: "jogomemoria-1b812.firebaseapp.com",
		// 	databaseURL: "https://jogomemoria-1b812.firebaseio.com",
		// 	projectId: "jogomemoria-1b812",
		// 	storageBucket: "jogomemoria-1b812.appspot.com",
		// 	messagingSenderId: "817010799690"
		// });

		$transitions

			.onBefore(null, (transition) => {

		// 		if(!$rootScope.basicInfo.name && transition.to().name == 'root.default.game') {

		// 			alert('É preciso informar seu nome antes de começar.');

		// 			return $state.target('root.default.initial', undefined, { location: true });

		// 		}

				if (localStorage.getItem('logado') && transition.to().name == 'root.authentication.login') {
					transition.abort();
					$location.path('/');
				}

				if (!localStorage.getItem('logado') && transition.params().authenticate) {
					transition.abort();
					$location.path('/login');
				}
					
					// authenticate
			});

	}]);
